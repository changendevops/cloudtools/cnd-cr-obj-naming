import cndprint


_level = "trace"
_silent_mode = True
_print = cndprint.CndPrint(level=_level, silent_mode=_silent_mode)
_id = 123

_consul = {
	"host": "127.0.0.1",
	"port": "8500",
	"token": "2ecf5aea-9137-b22d-86dc-532908fe30a1",
	"path": "history/vraX/deployments"
}
_data = {
	"env": "d",
	"service_name": "mysql_cluster",
	"deployment_id": "e6357e60-d74d-4976-9473-da3ff147f600",
	"vra_id": "x"
}
_name = "ariax-mysql_cluster-d-a0bcca9df17f"
