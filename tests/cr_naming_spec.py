from mockito import when, mock, unstub
from expects import *
from mamba import description, context, it
import cr_obj_naming
import tests.vars as vars


_cnd_consul = cr_obj_naming.CndConsul(
    vars._consul["host"],
    vars._consul["port"],
    vars._consul["token"],
    vars._consul["path"],
    vars._print,
    scheme='http'
)

with description("CrObject") as self:
    with before.each:
        unstub()
        self.cr_obj_naming = cr_obj_naming.ObjNaming(vars._data, _cnd_consul, 12, vars._print)
        
    with context('__init__'):
        with it('should set _print'):
            expect(self.cr_obj_naming._print).to(equal(vars._print))
        
    with context('save'):
        with it('should ssave'):
            when(_cnd_consul).put(...).thenReturn(True)
            expect(self.cr_obj_naming.save()).to(equal(vars._name))
        
    with context('find_by_id'):
        with it('should  return name'):
            when(_cnd_consul).get(...).thenReturn(vars._data)
            # when(_cnd_consul).get(...).thenReturn([1, {'LockIndex': 0, 'Key': 'history/vraX/deployments/3bfdc154-5e7c-4b7a-9615-9ab8c7f82dfa', 'Flags': 0, 'Value': b'{\n    "env": "d",\n    "service_name": "mysql_cluster",\n    "deployment_id": "3bfdc154-5e7c-4b7a-9615-9ab8c7f82dfa",\n    "vra_id": "x",\n    "name": "x-mysql_cluster-d-c12d60efb6dd"\n}', 'CreateIndex': 951, 'ModifyIndex': 951}])
            expect(self.cr_obj_naming.find_by_id(vars._data["deployment_id"])).to(equal(vars._data))
        
    with context('update'):
        with it('should update'):
            data = self.cr_obj_naming.data
            data['env'] = 'z'
            self.cr_obj_naming.data = data
            when(_cnd_consul).put(...).thenReturn(True)
            upt = self.cr_obj_naming.update()
            expect(upt).to(equal(self.cr_obj_naming.data["name"]))
        
    with context('destroy'):
        with it('should return true'):
            when(_cnd_consul).destroy(...).thenReturn(True)
            expect(self.cr_obj_naming.destroy()).to(equal(True))
        
    with context('has_children'):
        with it('should return false'):
            expect(self.cr_obj_naming.has_children()).to(equal(False))
        
    with context('find_relation'):
        with it('should return []'):
            expect(self.cr_obj_naming.find_relation()).to(equal([]))
        
    with context('all'):
        with it('should return all'):
            result = [{"deployment_id": '3bfdc154-5e7c-4b7a-9615-9ab8c7f82dfa'}, {"deployment_id": 'e6357e60-d74d-4976-9473-da3ff147f600'}]
            when(_cnd_consul).all(...).thenReturn(result)
            expect(self.cr_obj_naming.all()).to(equal(result))
