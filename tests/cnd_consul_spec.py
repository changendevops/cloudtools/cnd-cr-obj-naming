from mockito import when, mock, unstub
from expects import *
from mamba import description, context, it
import cr_obj_naming
import tests.vars as vars
import json


with description("CndConsul") as self:
    with before.each:
        unstub()
        self.cnd_consul = cr_obj_naming.CndConsul(
            vars._consul["host"],
            vars._consul["port"],
            vars._consul["token"],
            vars._consul["path"],
            vars._print
        )
        
    with context('__init__'):
        with it('should set _print'):
            expect(self.cnd_consul._print).to(equal(vars._print))
        
    with context('full_path'):
        with it('should return full_path with id'):
            path = self.cnd_consul.full_path('id')
            expect(path).to(equal('history/vraX/deployments/id'))

        with it('should return full_path without id'):
            path = self.cnd_consul.full_path()
            expect(path).to(equal('history/vraX/deployments/'))
        
    with context('all'):
        with it('should return all the id'):
            result = [{"Value": json.dumps({"a": "b"})}, {"Value": json.dumps({"c": "d"})}]
            when(self.cnd_consul.consul.kv).get(...).thenReturn([0, result])
            paths = self.cnd_consul.all()
            expect(paths).to(equal([{"a": "b"}, {"c": "d"}]))

        with it('should return all the id'):
            when(self.cnd_consul.consul.kv).get(...).thenReturn([0, []])
            paths = self.cnd_consul.all()
            expect(paths).to(equal([]))
        
    with context('get'):
        with it('should return all the id'):
            when(self.cnd_consul.consul.kv).get(...).thenReturn([1, {'LockIndex': 0, 'Key': 'history/vraX/deployments/e6357e60-d74d-4976-9473-da3ff147f600', 'Flags': 0, 'Value': b'{\n    "env": "d",\n    "service_name": "mysql_cluster",\n    "deployment_id": "e6357e60-d74d-4976-9473-da3ff147f600",\n    "vra_id": "x",\n    "name": "x-mysql_cluster-d-c12d60efb6dd"\n}', 'CreateIndex': 951, 'ModifyIndex': 951}])
            paths = self.cnd_consul.get()
            del paths["name"]
            expect(paths).to(equal(vars._data))
        
    with context('put'):
        with it('should put content and return True'):
            when(self.cnd_consul.consul.kv).put(...).thenReturn(True)
            result = self.cnd_consul.put('id', {})
            expect(result).to(equal(True))
        
    with context('destroy'):
        with it('should destroy item'):
            when(self.cnd_consul.consul.kv).delete(...).thenReturn(True)
            result = self.cnd_consul.destroy('id')
            expect(result).to(equal(True))